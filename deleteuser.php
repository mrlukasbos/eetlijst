<? session_start();

$currenthouseid = $_SESSION['houseid'];
$currenthousename = $_SESSION['housename'];
?>

<!-- Modal -->


<?php 
	ob_start();
include('databaseconnect.php');
?> 

<div class="modal fade" id="deleteusermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Huisgenoten verwijderen</h4>
            </div>

            <div class="modal-body">
				
				<!-- create a table, then store the users there with checkboxes --> 
               <table> 
				<?php 
				$records = array();
	
				/* check connection */
				if ($mysqli->connect_errno) {
					printf("Connect failed: %s\n", $mysqli->connect_error);
					exit();
				}

				/* Select queries return a resultset */
				if ($result = $mysqli->query("SELECT `name`, `id` FROM `users` WHERE `houseid`='$currenthouseid'")) {
					while ($row = mysqli_fetch_assoc($result)) {
				   ?> 
				  	   
				   <!-- create a new tablerow --> 
				   <tr>
					   <!-- the first element --> 
				
					   <td>  <div class="checkbox">
						   <label>
							   <input type="checkbox" name="<?php echo $row["id"]; ?>" class="dltcheckbox" value="test">
						   </label>
						   </div> </td>
					   <td><?php echo $row["name"]; ?></td>
					   <td><?php //echo $row["id"]; ?></td>
				   </tr>	
				   <?php 
					 }
				} 
				?>
			 </table>
				</div>
           	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-danger" onclick="deleteUsers()" type="button">Verwijderen</button>
      </div>
 
        </div>
    </div>
</div>



<script type="text/javascript">
	
	//this function will be done on submit. 
	
function deleteUsers() {
var checkedboxes = document.getElementsByClassName("dltcheckbox");
var checkednames = [];
	
for(var i=0; i<checkedboxes.length; i++) {
	if (checkedboxes[i].checked == true) { //check all checked boxes
		 $checkedname = checkedboxes[i].name; //get corresponding names 
		checkednames.push($checkedname);  //add to array
	}
}
	
  $.ajax({
           type: "POST",
           url: 'ajax.php',
           data:{action:'delete_users',
			vals: checkednames},
           success:function(vals){

               $('#deleteusermodal').modal('hide');
               $(".tableholder").load("table.php");


               $(".alertholder").html("<div class=\"alert alert-success fade in\" role=\"alert\"><b>Success! </b>User deleted.</div>");
               dismissAlert();

               $(".deleteusermodalholder").load("deleteuser.php");

           }

      });
}
</script>

