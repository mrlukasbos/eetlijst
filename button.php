<? include('functions.php'); ?>

<script>
	//create function for a button
	function Button(fid)
{
    this.id=fid;
    this.count = 0; 
	this.setCount = function(inputcount){ 
		this.count = inputcount; 
		this.changeContent('locked');
	}
	
	this.updateClick = function(c) {				
	
		
		 if (this.count < 3) {
				this.count++; 
		} else {
				this.count = 0;
		}
		//document.write(this.count + this.id);
		this.changeContent('unlocked');
	}
	
	this.updateDropdown = function(label) {
		
		switch(label) { 
			case "nothing":
				this.count=0;
			break;
			case "yes":
				this.count=1; 
			break;
			case "no":
				this.count=2; 
			break;
			case "cook":
				this.count=3; 
			break;
			case "other":
				//request a modal over here. (where user can insert how many people will eat).
				this.count=4; 
			break;
		}	
		this.changeContent('unlocked');
	}
			
	this.changeContent = function(initiallock) {
	id = "#"+fid;
		
		switch(this.count) { 
			case 0: 
				$showtext = "???";
				$(id).addClass('btn-default').removeClass('btn-success btn-danger btn-primary btn-warning');
				$(id+"drop").addClass('btn-default').removeClass('btn-success btn-danger btn-primary btn-warning');	
			break;
			case 1: 
				$showtext = "Ja!";
				$(id).addClass('btn-success').removeClass('btn-default btn-danger btn-primary btn-warning');
				$(id+"drop").addClass('btn-success').removeClass('btn-default btn-danger btn-primary btn-warning');
			break;
			case 2: 
				$showtext = "Nee";
				$(id).addClass('btn-danger').removeClass('btn-default btn-success btn-primary btn-warning');
				$(id+"drop").addClass('btn-danger').removeClass('btn-default btn-success btn-primary btn-warning');
			break;
			case 3: 
				$showtext = "Kok";
				$(id).addClass('btn-primary').removeClass('btn-default btn-success btn-danger btn-warning');
				$(id+"drop").addClass('btn-primary').removeClass('btn-default btn-success btn-danger btn-warning');
			break;
			case 4: 
				$showtext = "*";
				$(id).addClass('btn-warning').removeClass('btn-default btn-success btn-danger btn-primary');
				$(id+"drop").addClass('btn-warning').removeClass('btn-default btn-success btn-danger btn-primary');
			break;
		}
		$(id+":first-child").html($showtext);

        if (initiallock == 'unlocked') {
            saveBtnData(fid, this.count);
        }
	}	
}
</script>