<? session_start() ?>

<!-- Modal -->


<div class="modal fade" id="deletehousemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">HUIS VERWIJDEREN</h4>
            </div>

            <div class="modal-body">

                <p> Weet u zeker dat u uw huis uit de database wil halen? Dit kan niet ongedaan gemaakt worden.</p>

                <!-- The form is placed inside the body of modal -->
                <form id="deletehouseform" method="post" class="form-horizontal" action="" onsubmit="">


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="deletehousecheckbox" class="dlthousecheckbox" id="deletehousecheckbox" value="test">
                            Ik weet wat ik doe.
                        </label>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-0">
                            <button type="submit" class="btn btn-danger">Huis verwijderen</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    type="text/javascript">



    $("#deletehouseform").submit(function() {

        if ($('#deletehousecheckbox').prop('checked')) {

            $.ajax({
                type: "POST",
                url: 'ajax.php',
                data: {
                    action: 'delete_house'
                },
                success: function(vals)
                {

                    window.location.href = "cover.php";
                } });

    } else {

        $('#deletehousemodal').modal('hide');


        $(".alertholder").html("<div class=\"alert alert-warning fade in\" role=\"alert\"><b>Fjoe! </b>Dat scheelde weinig.</div>");
        dismissAlert();
        }



        return false; // avoid to execute the actual submit of the form.
    });


</script>