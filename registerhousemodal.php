

<!-- Modal -->


<div class="modal fade" id="registerhousemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Een nieuw huis toevoegen</h4>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="registerhouseform" method="post" class="form-horizontal" action="" onsubmit="">

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Huis naam</label>
                        <div class="col-xs-5">
                            <input type="text" id="namefield" class="form-control" name="housename" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Huis wachtwoord</label>
                        <div class="col-xs-5">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-success">Ik ben er klaar voor!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    type="text/javascript">

    function InputChecker()
    {
        if(document.getElementById('namefield') == '')  { //  empty
            alert("This element needs data"); // Pop an alert
            return false; // Prevent form from submitting
        }
    }


    $("#registerhouseform").submit(function() {

        $.ajax({
            type: "POST",
            url: 'ajax.php',
            data: {
                vals: $("#registerhouseform").serialize(), // serializes the form's elements
                action: 'register_house'},
            success: function(vals)
            {
                $('#registerhouseform').modal('hide');
                window.location.href = "cover.php";

            }
        });

        return false; // avoid to execute the actual submit of the form.
    });


</script>