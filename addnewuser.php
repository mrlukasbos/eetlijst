

<!-- Modal -->


<div class="modal fade" id="addusermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Huisgenoot toevoegen</h4>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="adduserform" method="post" class="form-horizontal" action="" onsubmit="">
                    
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Naam</label>
                        <div class="col-xs-5">
                            <input type="text" id="namefield" class="form-control" name="name" />
                        </div>
                    </div>
					
					 <div class="form-group">
                        <label class="col-xs-3 control-label">Email</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="password" />
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

	type="text/javascript">
		
function InputChecker()
{
    if(document.getElementById('namefield') == '')  { //  empty
        alert("This element needs data"); // Pop an alert
        return false; // Prevent form from submitting
    }
}


    $("#adduserform").submit(function() {

        $.ajax({
            type: "POST",
            url: 'ajax.php',
            data: { vals: $("#adduserform").serialize(), // serializes the form's elements.
            action: 'add_users'},
            success: function(vals)
            {
                $('#addusermodal').modal('hide');

                // we need to update the delete user modal also
                $(".deleteusermodalholder").load("deleteuser.php");
                //reload the table.
                $(".tableholder").load("table.php");



                //show alert.
                $(".alertholder").html("<div class=\"alert alert-success fade in\" role=\"alert\"><b>Success! </b>User added.</div>");
                dismissAlert();
            }
        });

        return false; // avoid to execute the actual submit of the form.
    });


</script>