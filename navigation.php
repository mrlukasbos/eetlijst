<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Eetlijst</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <div class="alertholder"> </div>

            <ul class="nav navbar-nav navbar-right">
                <!-- <button type="button" id="saveeatdatebtn" class="tablebtndrop btn btn-md btn-success dropdown-toggle" onclick="saveData()" >Eetgegevens opslaan</button> -->
                <li><a href="#" data-toggle="dropdown" aria-expanded="false">Huis</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="" data-toggle="modal" data-target="#addusermodal">Huisgenoten toevoegen</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#deleteusermodal">Huisgenoten verwijderen</a></li>
                        <li class="divider"></li>
                        <li><a href="#" data-toggle="modal" data-target="#deletehousemodal">Huis verwijderen</a></li>
                    </ul>
                </li>

                <li><a href="logout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>