<?php session_start();

$currenthouseid = $_SESSION['houseid'];
$currenthousename = $_SESSION['housename'];
?>


<div class="maintable col-md-8">
    <?php include('databaseconnect.php'); ?>
    <?php date_default_timezone_set("Europe/Amsterdam"); ?>


    <div class="table-responsive">
				<table class="table table-striped">
					<tbody>
			 			<?php $records = array();

                       // $currenthouseid = $_SESSION['houseid'];
                        //$currenthousename = $_SESSION['housename'];

                        //echo $_SESSION['housename'];

						/* Select queries return a resultset */
						if ($result = $mysqli->query("SELECT name, id FROM users WHERE houseid = '$currenthouseid' ")) {
                        $storedbuttondata =  $mysqli->query("SELECT * FROM data WHERE houseid = '$currenthouseid' ");
                        $lsd =  $mysqli->query("SELECT * FROM dates ");

                        ?>

						<!--create the upper tablerow -->
					<tr> 	
						
						<!--The first element will be empty, because that column is for the names --> 
                        <th></th>

                        <?php
                            $date = new DateTime(date('Y-m-d'));
                         $currentdate = $date->format('Y-m-d');


                        $lastdate = mysqli_fetch_assoc($lsd)["lastsaveddate"];

                       //$lastdate =  "2015-3-01";

                        $diff = strtotime($currentdate) - strtotime($lastdate);


                        $days = floor($diff / (60*60*24));


                       // printf(" Last updated: ".$days." days ago. ");

                        //the first header will be filled with names.
                        $usercount = 0;

                        while ($row = mysqli_fetch_assoc($result)) {
                            $names[count($names)] = $row['name'];
                            $ids[count($ids)] = $row['id'];

                        }
                        for ($i = 0; $i<count($names); $i++) {
                        ?>

					    <th class=""> <?php echo $names[$i]; ?> </th>

                        <? } ?>

                    </tr>
                        <script> var btn = new Array(); </script>

                        <? for ($x = 0; $x <= 30; $x++) {
                            ?> <tr>
                                <td>
                            <?
                            if ($date->format(d) == 1 && $date->format(m) == 1 || $x == 0) {
                                echo $date->format('Y'); ?> <br> <?php
                                echo $date->format('F'); ?> <br> <?php

                            } else if ($date->format(d) == 1) {
                                echo $date->format('F'); ?> <br> <?php
                            }
                            echo $date->format('D d') . "\n";
                            ?>

                            </td>

                                <?   for ($i = 0; $i<count($names); $i++) {
                                    $id = $ids[$i]."-".($x); //the from a button is the name + the position of the button.
                                    ?>
                                    <td>
                                        <script>
                                            btn['<?php echo $id; ?>'] = new Button("<?php echo $id; ?>");
                                        </script>

                                        <!-- here we are creating the buttons, uncluding the dropdowns -->
                                        <div class="btn-group">
                                            <button
                                                type="button"
                                                id="<?php echo $id; ?>"
                                                class="tablebtn btn btn-xs btn-default"
                                                onclick="btn['<?php echo $id; ?>'].updateClick()">???
                                            </button>

                                            <button
                                                type="button"
                                                id="<?php echo $id."drop"; ?>"
                                                class="tablebtndrop btn btn-xs btn-default dropdown-toggle"
                                                data-toggle="dropdown"
                                                aria-expanded="false" >
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>

                                            <ul class="tabledropdown dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#"
                                                       class="cook"
                                                       onclick="btn['<?php echo $id; ?>'].updateDropdown('nothing')">???
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                       class="yes"
                                                       onclick="btn['<?php echo $id; ?>'].updateDropdown('yes')">Ja!
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                       class="no"
                                                       onclick="btn['<?php echo $id; ?>'].updateDropdown('no')">Nee
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                       class="cook"
                                                       onclick="btn['<?php echo $id; ?>'].updateDropdown('cook')">Kok
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#"
                                                       class="other"
                                                       onclick="btn['<?php echo $id; ?>'].updateDropdown('other')">Overig
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                <? }  ?>
                            </tr>
						<?php
						$date->modify('+1 day');  
						}

                    while ($dataid = mysqli_fetch_assoc($storedbuttondata)) {

                                $oldid = explode("-", $dataid["btnid"]);
                                $idcount = $oldid[1];
                                $idname = $oldid[0];
                                $newidcount = (int)$idcount - $days;
                                $newid = $idname . "-" . $newidcount;

                                if ($newidcount >= 0 && $newidcount <= 30) {


                        ?>


                            <script>
                                if (btn['<?php echo $newid ?>'] != null) {

                                    btn['<?php echo $newid ?>'].setCount(<?php echo $dataid["btnvalue"]?>);
                                }
                            </script>  <?php

                        }
                            }

				$result->close();
                            $storedbuttondata->close();
                                $lsd->close();
			} 
		?> 
          </tbody>    
      </table>
            </div>
</div>



