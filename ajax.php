<? session_start();

$currenthouseid = $_SESSION['houseid'];
$currenthousename = $_SESSION['housename'];?>

<?php
if($_POST['action'] == 'delete_users') {

    include('databaseconnect.php');


    $array = $_POST['vals'];
	for($i=0;$i<count($array); $i++) {

		// attempt insert query execution
		$sql = "DELETE FROM users WHERE id='$array[$i]'";

		if(mysqli_query($mysqli, $sql)){

			echo $array[$i]." deleted successfully.";	

		} else{
			echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
		}
	}
	// close connection
	mysqli_close($mysqli);
}

if($_POST['action'] == 'add_users') {
    include('databaseconnect.php');


    $tempval = (explode("&", $_POST["vals"])); // returns something like array[0] = "name=example"
    $tempname = (explode("=", $tempval[0]));
    $tempmail = (explode("=", $tempval[1]));


// Escape user inputs for security
    $name = mysqli_real_escape_string($mysqli, $tempname[1]);
    $email = mysqli_real_escape_string($mysqli,  $tempmail[1]);

    $house = $currenthouseid;

// attempt insert query execution
    $sql = "INSERT INTO `users`(`name`, `email`, `houseid`) VALUES ('$name','$email','$house')";

    if(mysqli_query($mysqli, $sql)){
         echo "Records added successfully.";

        //  header("Location: index.php?useradded=yes");

    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
    }
    // close connection
    mysqli_close($mysqli);
}

if($_POST['action'] == 'add_frontpagetext') {
    include('databaseconnect.php');

    $truncatefrontpagequery = "DELETE FROM frontpagetextfield WHERE houseid='$currenthouseid'";


    if(mysqli_query($mysqli, $truncatefrontpagequery)){
        echo "success!";
    } else{
        echo "ERROR: Could not able to execute $truncatefrontpagequery. " . mysqli_error($mysqli);
    }


// Escape user inputs for security
    $text = mysqli_real_escape_string($mysqli, $_POST['vals']);

    $house = $currenthouseid;

// attempt insert query execution
    $sql = "INSERT INTO `frontpagetextfield`(`frontpagetext`, `houseid`) VALUES ('$text', '$house')";

    if(mysqli_query($mysqli, $sql)){
        echo "Records added successfully.";

        //  header("Location: index.php?useradded=yes");

    } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
    }
    // close connection
    mysqli_close($mysqli);
}


if($_POST['action'] == 'save_data') {
	include('databaseconnect.php');

    $truncatedatesquery = "TRUNCATE TABLE `dates`";

    if(mysqli_query($mysqli, $truncatedatesquery)){
        echo "success!";
    } else{
        echo "ERROR: Could not able to execute $truncatedatesquery. " . mysqli_error($mysqli);
    }


     date_default_timezone_set("Europe/Amsterdam");
    $date = new DateTime(date('Y-m-d'));

    $lastdatequery = "INSERT INTO dates (`lastsaveddate`) VALUES (CURRENT_DATE())";

    if(mysqli_query($mysqli, $lastdatequery)){
        echo "success!";
    } else{
        echo "ERROR: Could not able to execute $lastdatequery. " . mysqli_error($mysqli);
    }


    //$truncatedataquery = "DELETE * FROM data WHERE houseid='$currenthouseid'";

	
	$buttonids = $_POST['btnids'];
	$buttondata = $_POST['btndata'];
			
	for($i=0;$i<count($buttondata); $i++) { 
		
		$buttonid = $buttonids[$i];
		$buttonint = $buttondata[$i];


		
		// attempt insert query execution
		
		$sql = "REPLACE INTO `data`(`btnid`, `btnvalue`, `houseid`) VALUES ('$buttonid', '$buttonint', '$currenthouseid')";

		
		if(mysqli_query($mysqli, $sql)){

			echo "success!";	

		} else{
			echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
		}
	}
}


if($_POST['action'] == 'save_btn_data') {
    include('databaseconnect.php');

    $btnid = mysqli_real_escape_string($mysqli, $_POST['buttonid']);
    $btncount = mysqli_real_escape_string($mysqli, $_POST['buttoncount']);

   $sql = $mysqli->query("REPLACE INTO `data`(`btnid`, `btnvalue`, `houseid`) VALUES ('$btnid', '$btncount', '$currenthouseid')");

    if(mysqli_query($mysqli, $sql)){

        echo "success!";

    } else{
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
    }

}

if($_POST['action'] == 'user_try_login') {
    include('databaseconnect.php');


    $housename = mysqli_real_escape_string($mysqli, $_POST['hsnm']);
    $pass = mysqli_real_escape_string($mysqli, $_POST['pwd']);



    $result = $mysqli->query("select * from houses where housename='$housename' and housepassword='$pass'");
    $rows = mysqli_num_rows($result);
   // echo("$rows");
        if ($rows == 1) //***if the userName and password matches then register a session and redrect user to the Successfull.php
        {
            $_SESSION['housename'] = $housename;

            $_SESSION['password'];

            while ($row = mysqli_fetch_assoc($result)) {
                $_SESSION['houseid'] = $row["houseid"];
            }

           // echo $_SESSION['housename'];
            exit('success');
        } else {
            echo 'Username and/or password is incorrect.';
          //  exit('error');

        }
}

if($_POST['action'] == 'delete_house') {
include('databaseconnect.php');


    $tables = array("houses","users","frontpagetextfield","data");
    foreach($tables as $table) {
        $query = "DELETE FROM $table WHERE houseid='$currenthouseid'";
        mysqli_query($mysqli,$query);
    }


    mysqli_close($mysqli);

}


if($_POST['action'] == 'register_house') {
include('databaseconnect.php');

    $tempval = (explode("&", $_POST["vals"])); // returns something like array[0] = "name=example"
    $tempname = (explode("=", $tempval[0]));
    $temppass = (explode("=", $tempval[1]));


// Escape user inputs for security
    $housename = mysqli_real_escape_string($mysqli, $tempname[1]);
    $housepass = mysqli_real_escape_string($mysqli,  $temppass[1]);


    $sql = "INSERT INTO `houses`(`housename`, `housepassword`) VALUES ('$housename', '$housepass')";


    if(mysqli_query($mysqli, $sql)){

        echo "success!";

    } else{
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
    }
}

?> 

