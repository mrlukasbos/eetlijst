<? session_start();

$currenthouseid = $_SESSION['houseid'];
$currenthousename = $_SESSION['housename'];

include('databaseconnect.php');
$frontpagetextq =  $mysqli->query("SELECT * FROM frontpagetextfield WHERE `houseid` = '$currenthouseid'");

while ($frontpagetextinfo = mysqli_fetch_assoc($frontpagetextq)) {
    $frontpagetext = $frontpagetextinfo['frontpagetext'];
}
?>

<div class="info col-md-4">
    <h1 class="sub-header"><? echo $currenthousename; ?></h1>

    <form id="frontpageform" method="post" class="form-horizontal" action="" onsubmit="">

        <div class="form-group">
            <label class="col-xs-1 control-label">Prikbord</label>

            <div class="col-md-12">
                <textarea id="frontpagetext" class="form-control" rows="7"><? echo $frontpagetext; ?></textarea>

            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12 ">
                <button type="submit" class="btn btn-success">Prikbord updaten</button>
            </div>
        </div>
    </form>

</div>
